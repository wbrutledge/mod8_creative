Ext.DomHelper.useDom = true; // prevent XSS

var socket = io.connect(window.location.origin);

var commentData, myuser, myusername, myuserid, mysocket, who, myGame, curWord, guesses, correct;

//socket.IO javascript

Ext.onReady(function() {
	
	
	myusername = readCookie("usernameCookie");
	if (null==myusername) {
		//user has logged out, kick back to beginning
		window.location.replace("http://ec2-54-200-63-85.us-west-2.compute.amazonaws.com/~blake/mod8/vers1/logout.php");
	}
	
	deleteCookie("usernameCookie");
	socket.emit("newcomer", myusername);
	socket.emit("popLobby", myusername);
	socket.emit("updateOpponents", myusername);
		
	socket.on("myProfile", function(data, socketid){
		myuser = data;
		mysocket = socketid;
		//console.log(myuser);
		Ext.get("myProfile_rank").dom.innerHTML = myuser.rank;
		Ext.get("myProfile_name").dom.innerHTML = myuser.username;
		Ext.get("myProfile_wins").dom.innerHTML = myuser.wins;
		Ext.get("myProfile_losses").dom.innerHTML = myuser.losses;
	});
	
	socket.on("askPopLobby", function(){
		socket.emit("popLobby", myusername);
	});
	
	socket.on("updateAvailable", function(data){
		//console.log(data);
		for(i=0; i<data.length; i++){
			var thisObj = data[i];
			//console.log(thisObj);
			if(! ((thisObj.username)==myusername)){
				//this opponent is available and is not me
				var temp = "challenge_"+thisObj.username;
				Ext.get("available-opponents-table").createChild({
					tag: "tr",
					id: temp,
					children: [" "]				
				});
				var thisRank = thisObj.rank;
				var thisName = thisObj.username;
				var thisWins = thisObj.wins;
				var thisLosses = thisObj.losses;
				Ext.get(temp).dom.innerHTML = "<td class='table-rank' id='"+thisName+"'>"+thisRank+"</td><td class='table-name' id='"+thisName+"'>"+thisName+"</td><td class='table-wins' id='"+thisName+"'>"+thisWins+"</td><td class='table-losses' id='"+thisName+"'>"+thisLosses+"</td>"
			}
		}
	});
	
	socket.on("deleteAvailable", function(name){
		var temp = "challenge_"+name;
		if (!(document.getElementById(temp)==null)) {
			Ext.get(temp).remove();
		}
	});
	
	socket.on("leaderboardUpdate", function(data){
		leaders = data;
		for(var i = 0; i < 5; i++){
			var rankID = "leader"+i+"_rank";
			var nameID = "leader"+i+"_name";
			var winsID = "leader"+i+"_wins";
			var lossesID = "leader"+i+"_losses";
			var leaderobject = leaders[i];
			Ext.get(rankID).dom.innerHTML = leaderobject.rank;
			Ext.get(nameID).dom.innerHTML = leaderobject.username;
			Ext.get(winsID).dom.innerHTML = leaderobject.wins;
			Ext.get(lossesID).dom.innerHTML = leaderobject.losses;
		}
	});
	
	Ext.fly("send-challenge").on("click", function(){
		var propOpp = Ext.fly("challenge-input").getValue();
		socket.emit("challengingYou", propOpp, myusername);
	});
	
		
	socket.on("challengeMe", function(data){
		//console.log(data);
		var dataOut = data;
		var opp = data[0];
		var oppSock = data[1];
		var chal = data[2];
		var chalSock = data[3];
		//console.log("Hey, "+opp+"! You are being challenged by "+chal+" to a match!");
		var check = confirm("Hey, "+opp+"! You have been challenged to a match against "+chal+". Do you accept?");
		socket.emit("challengeAnswer", dataOut, check);
	});
	
	socket.on("challengeDenied", function(data){
		var opp = data[0];
		alert("Sorry, but "+opp+" has declined your challenge!");
	});
	
	Ext.fly("chat-submit").on("click", function() {
		comment = Ext.fly("chat-text").getValue();
		commentData = myusername+": "+comment;
		//Ext.fly("chat-text").setValue('');
		socket.emit("sendmessage", commentData);

		Ext.fly("chat-container").createChild({
				tag: "li",
				children: [commentData]
			});
		fillInputText("chat-text", "");
		var objDiv = document.getElementById("chat-container");
		objDiv.scrollTop = objDiv.scrollHeight;
	});
		
	socket.on("relaymessage", function(content){
		// This callback runs when the server sends us a "someonesaid" event
		Ext.fly("chat-container").createChild({
			tag: "li",
			children: [content]
		});
		var objDiv = document.getElementById("chat-container");
		objDiv.scrollTop = objDiv.scrollHeight;
	});
	
	socket.on("updatePlayers", function(content){
		Ext.get("chatHeader").dom.innerHTML = "Chat Room with "+content+" others.";
	});
	
	socket.on("alreadyLoggedIn", function(){
		notSuccess();
		return;
	});
		
	socket.on("getAllUsers", function(data) {
		console.log(data);
	});
	
	
	
	//load gameplay
	socket.on("initializeGame", function(game){
		myGame = game;
		document.getElementById("game-wrap").className = "";
		document.getElementById("game-wrap").className = "show";
		document.getElementById("challenge-wrap").className = "";
		document.getElementById("challenge-wrap").className = "noshow";
		document.getElementById("opponents-wrap").className = "";
		document.getElementById("opponents-wrap").className = "noshow";
		//console.log(game.id);
		//console.log(game.chal);
		Ext.get("chalName").dom.innerHTML = game.chal;
		Ext.get("oppName").dom.innerHTML = game.opp;
		Ext.get("chalScore").dom.innerHTML = game.chalScore;
		Ext.get("oppScore").dom.innerHTML = game.oppScore;
		
		//if i am chal, i go first
		if (game.chal==myusername) {
			who = "chal";
			//console.log("i am "+game.chal);
			Ext.get("scoreboard-notification").dom.innerHTML = "Submit a word!";
			document.getElementById("new-word-input-wrap").className = "";
			document.getElementById("new-word-input-wrap").className = "show";
			document.getElementById("letters-wrap").className = "";
			document.getElementById("letters-wrap").className = "noshow";
			guesses = 6;
			correct = 0;
			Ext.get("guessesLeft").dom.innerHTML = guesses;
		};
		//if i am opp, i dont
		if (game.opp==myusername) {
			guesses = 6;
			correct = 0;
			document.getElementById("new-word-input-wrap").className = "";
			document.getElementById("new-word-input-wrap").className = "noshow";
			Ext.get("guessesLeft").dom.innerHTML = guesses;
			who = "opp";
			//console.log("i am "+game.opp);
			Ext.get("scoreboard-notification").dom.innerHTML = "Wait to guess...";
		};
		
	});
	
	Ext.fly("submit-new-word").on("click", function() {
		console.log("client trying to sends new word request");
		var newWord = Ext.fly("new-word").getValue();
		//Ext.fly("chat-text").setValue('');
		socket.emit("submitNewWord", myGame, who, newWord);
		console.log("client sends new word request");
	});
	
	socket.on("invalidWord", function(){
		Ext.get("scoreboard-notification").dom.innerHTML = "Invalid word!";
		fillInputText("new-word", "");
	});
	
	socket.on("receiveNewWord", function(wordArray, wordString){
		curWord = wordArray;
		Ext.get("scoreboard-notification").dom.innerHTML = "Guess away!";
		for(var i=0;i<wordArray.length;i++) {
			console.log("inside loop");
			Ext.fly("answer-letter-list").createChild({
				tag: "li",
				class: "answer-letter",
				id: "letterSpace_"+i,
				children: ["_"]
			});
		};
	});
	
	socket.on("receiveNewWord_Watch", function(wordArray, wordString){
		curWord = wordArray;
		Ext.get("scoreboard-notification").dom.innerHTML = "Watching..";
		for(var i=0;i<wordArray.length;i++) {
			console.log("inside loop");
			Ext.fly("answer-letter-list").createChild({
				tag: "li",
				class: "answer-letter",
				id: "letterSpace_"+i,
				children: ["_"]
			});
		};
		document.getElementById("new-word-input-wrap").className = "";
		document.getElementById("new-word-input-wrap").className = "noshow";
	});
	
	socket.on("wrongGuess", function(letter, updatedGuesses){
		guesses = updatedGuesses;
		var negGuesses = 6-guesses;
		document.getElementById(letter).className = "";
		document.getElementById(letter).className = "letter tried";
		Ext.get("scoreboard-notification").dom.innerHTML = "Wrong guess!";
		Ext.get("guessesLeft").dom.innerHTML = guesses;
		for(var i=0; i<negGuesses; i++){
			console.log(i);
			//should do nothing until guess is down to 5
			var str = "man-"+i;
			document.getElementById(str).className = "";
			document.getElementById(str).className = "hangman-object lostLimb";
		};
	});
	
	socket.on("wrongGuess_Watch", function(letter, updatedGuesses){
		guesses = updatedGuesses;
		var negGuesses = 6-guesses;
		Ext.get("guessesLeft").dom.innerHTML = guesses;
		for(var i=0; i<negGuesses; i++){
			console.log(i);
			//should do nothing until guess is down to 5
			var str = "man-"+i;
			document.getElementById(str).className = "";
			document.getElementById(str).className = "hangman-object lostLimb";
		};
	});
	
	socket.on("rightGuess", function(letter, wordArray, updatedCorrect){
		correct = updatedCorrect;
		document.getElementById(letter).className = "";
		document.getElementById(letter).className = "letter correct";
		Ext.get("scoreboard-notification").dom.innerHTML = "Good one!";
		for(var i=0;i<wordArray.length;i++){
			if (wordArray[i]==letter) {
				//this is one of the letters guessed correctly
				Ext.get("letterSpace_"+i).dom.innerHTML = letter;
			};
		};
	});
	
	socket.on("rightGuess_Watch", function(letter, wordArray, updatedCorrect){
		correct = updatedCorrect;
		document.getElementById(letter).className = "";
		document.getElementById(letter).className = "letter correct";
		for(i=0;i<wordArray.length;i++){
			if (wordArray[i]==letter) {
				//this is one of the letters guessed correctly
				Ext.get("letterSpace_"+i).dom.innerHTML = letter;
			};
		};
	});
	
	
	//round over, switch
	
	socket.on("wordFail", function(letter, wordArray){
		Ext.get("guessesLeft").dom.innerHTML = 0;
		document.getElementById(letter).className = "";
		document.getElementById(letter).className = "letter tried";
		Ext.get("scoreboard-notification").dom.innerHTML = "No points!";
		for(var i=0; i<6; i++){
			console.log(i);
			//should do nothing until guess is down to 5
			var str = "man-"+i;
			document.getElementById(str).className = "";
			document.getElementById(str).className = "hangman-object lostLimb";
		};
		switchToWatcher();
	});
	
	socket.on("wordFail_Watch", function(letter, wordArray){
		Ext.get("guessesLeft").dom.innerHTML = 0;
		Ext.get("scoreboard-notification").dom.innerHTML = "Stumped 'em!";
		for(var i=0; i<6; i++){
			console.log(i);
			//should do nothing until guess is down to 5
			var str = "man-"+i;
			document.getElementById(str).className = "";
			document.getElementById(str).className = "hangman-object lostLimb";
		};
		switchToGuesser();
	});
	
	socket.on("wordWin", function(letter, wordArray, game){
		for(var i=0; i<6; i++){
			console.log(i);
			//should do nothing until guess is down to 5
			var str = "man-"+i;
			document.getElementById(str).className = "";
			document.getElementById(str).className = "hangman-object correctWord";
		};
		myGame = game;
		document.getElementById(letter).className = "";
		document.getElementById(letter).className = "letter correct";
		Ext.get("scoreboard-notification").dom.innerHTML = "Points!";
		for(i=0;i<wordArray.length;i++){
			if (wordArray[i]==letter) {
				//this is one of the letters guessed correctly
				Ext.get("letterSpace_"+i).dom.innerHTML = letter;
			};
		};
		updateScore(myGame);
		switchToWatcher();
	});
	
	socket.on("wordWin_Watch", function(letter, wordArray, game){
		for(var i=0; i<6; i++){
			console.log(i);
			//should do nothing until guess is down to 5
			var str = "man-"+i;
			document.getElementById(str).className = "";
			document.getElementById(str).className = "hangman-object correctWord";
		};
		myGame = game;
		Ext.get("scoreboard-notification").dom.innerHTML = "Too easy!";
		for(i=0;i<wordArray.length;i++){
			if (wordArray[i]==letter) {
				//this is one of the letters guessed correctly
				Ext.get("letterSpace_"+i).dom.innerHTML = letter;
			};
		};
		updateScore(myGame);
		switchToGuesser();
	});
	
	socket.on("returnToLobbyWin", function(){
		document.getElementById("game-wrap").className = "";
		document.getElementById("game-wrap").className = "noshow";
		document.getElementById("challenge-wrap").className = "";
		document.getElementById("challenge-wrap").className = "show";
		document.getElementById("opponents-wrap").className = "";
		document.getElementById("opponents-wrap").className = "show";
		document.getElementById("letters-wrap").className = "";
		document.getElementById("letters-wrap").className = "show";
		document.getElementById("new-word-input-wrap").className = "";
		document.getElementById("new-word-input-wrap").className = "show";
		fillInputText("challenge-input", "");
	});
	
	socket.on("returnToLobbyLoss", function(revenge){
		document.getElementById("game-wrap").className = "";
		document.getElementById("game-wrap").className = "noshow";
		document.getElementById("challenge-wrap").className = "";
		document.getElementById("challenge-wrap").className = "show";
		document.getElementById("opponents-wrap").className = "";
		document.getElementById("opponents-wrap").className = "show";
		document.getElementById("letters-wrap").className = "";
		document.getElementById("letters-wrap").className = "show";
		document.getElementById("new-word-input-wrap").className = "";
		document.getElementById("new-word-input-wrap").className = "show";
		fillInputText("challenge-input", revenge);
	});
});

//redraw board functions

function updateScore(game){
	myGame = game;
	//update scoreboard
	Ext.get("chalScore").dom.innerHTML = game.chalScore;
	Ext.get("oppScore").dom.innerHTML = game.oppScore;
	//check if game is over, first to 3, win by two
	if((game.chalScore>(game.oppScore+1))&&(game.chalScore>2)&&(who=="chal")){
		//chal wins! and im chal
		socket.emit("gameover", "chal", myGame, myuser.wins);
	}
	if((game.oppScore>(game.chalScore+1))&&(game.oppScore>2)&&(who=="opp")){
		//opp wins! and im opp
		socket.emit("gameover", "opp", myGame, myuser.wins);
		console.log("i won. -"+myusername);
	}
	if((game.oppScore>(game.chalScore+1))&&(game.oppScore>2)&&(who=="chal")){
		//i am chal and i lose
		socket.emit("iLost", "chal", myusername, myuser.losses, game.opp);
		console.log("i lost. -"+myusername);
	}
	if((game.chalScore>(game.oppScore+1))&&(game.chalScore>2)&&(who=="opp")){
		//i am opp and i lose
		socket.emit("iLost", "opp", myusername, myuser.losses, game.chal);
	}
	//game is not over! play on...
}

function switchToWatcher(){
	Ext.get("scoreboard-notification").dom.innerHTML = "Submit a word!";
	document.getElementById("new-word-input-wrap").className = "";
	document.getElementById("new-word-input-wrap").className = "show";
	document.getElementById("letters-wrap").className = "";
	document.getElementById("letters-wrap").className = "noshow";
	Ext.get("answer-letter-list").dom.innerHTML = "<!--hackCity-->";
	flushManColors();
	resetLetters();
	fillInputText("new-word", "");
	guesses = 6;
	correct = 0;
	Ext.get("guessesLeft").dom.innerHTML = guesses;
};

function switchToGuesser() {
	document.getElementById("letters-wrap").className = "";
	document.getElementById("letters-wrap").className = "show";
	document.getElementById("new-word-input-wrap").className = "";
	document.getElementById("new-word-input-wrap").className = "noshow";
	Ext.get("answer-letter-list").dom.innerHTML = "<!--hackCity-->";
	Ext.get("scoreboard-notification").dom.innerHTML = "Wait to guess...";
	flushManColors();
	resetLetters();
	fillInputText("new-word", "");
	guesses = 6;
	correct = 0;
	Ext.get("guessesLeft").dom.innerHTML = guesses;
};

function resetLetters() {
	//make all of the letters gray again
	Ext.get("letters-bank").dom.innerHTML = "<li class='letter avail' id='A'>A</li><li class='letter avail' id='B'>B</li><li class='letter avail' id='C'>C</li><li class='letter avail' id='D'>D</li><li class='letter avail' id='E'>E</li><li class='letter avail' id='F'>F</li><li class='letter avail' id='G'>G</li><li class='letter avail' id='H'>H</li><li class='letter avail' id='I'>I</li><li class='letter avail' id='J'>J</li><li class='letter avail' id='K'>K</li><li class='letter avail' id='L'>L</li><li class='letter avail' id='M'>M</li><li class='letter avail' id='N'>N</li><li class='letter avail' id='O'>O</li><li class='letter avail' id='P'>P</li><li class='letter avail' id='Q'>Q</li><li class='letter avail' id='R'>R</li><li class='letter avail' id='S'>S</li><li class='letter avail' id='T'>T</li><li class='letter avail' id='U'>U</li><li class='letter avail' id='V'>V</li><li class='letter avail' id='W'>W</li><li class='letter avail' id='X'>X</li><li class='letter avail' id='Y'>Y</li><li class='letter avail' id='Z'>Z</li>'";
};

function flushManColors() {
	console.log("getting heeeeeeere");
	Ext.get("hangman-wrap").dom.innerHTML = "<div class='noose-object' id='noose-poll'></div><div class='noose-object' id='noose-bar'></div><div id='noose-rope'></div><div class='hangman-object' id='man-0'></div><div class='hangman-object' id='man-1'></div><div class='hangman-object' id='man-2'></div><div class='hangman-object' id='man-3'></div><div class='hangman-object' id='man-4'></div><div class='hangman-object' id='man-5'></div><div id='eye1'></div><div id='eye2'></div><div class='noose-object' id='noose-base'></div>";
};

//event listeners

document.getElementById("letters-bank").addEventListener('click', deduceLetter, false);

document.getElementById("available-opponents-table").addEventListener('click', deduceOpponent, false);

function deduceLetter(event) {
	var id = event.target.id;
	if (!(id=="letters-bank")){
	   //console.log(id);
	   socket.emit("guessLetter", myGame, who, id, curWord, guesses, correct);
	}
};

function deduceOpponent(event){
	var id = event.target.id;
	//console.log(id);
	fillInputText("challenge-input", id);
};







//basic javascript

function notSuccess() {
	window.location.replace("http://ec2-54-200-63-85.us-west-2.compute.amazonaws.com/~blake/mod8/vers1/index.php?error-note=5");
};

function scrollDown(object){
	console.log("getting here: "+object);
	var objDiv = document.getElementById("object");
	objDiv.scrollTop = objDiv.scrollHeight;
};

function readCookie(name){
  var parts = document.cookie.split(name + "=");
  if (parts.length == 2) return parts.pop().split(";").shift();
};

function deleteCookie(string){
	document.cookie = string + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
};

function setCookie(c_name,value,exdays){
	var exdate=new Date();
	exdate.setDate(exdate.getDate() + exdays);
	var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
	document.cookie=c_name + "=" + c_value;
};

function fillInputText(element, string) {
	document.getElementById(element).value=string;
};

Element.prototype.remove = function() {
    this.parentElement.removeChild(this);
};
NodeList.prototype.remove = HTMLCollection.prototype.remove = function() {
    for(var i = 0, len = this.length; i < len; i++) {
        if(this[i] && this[i].parentElement) {
            this[i].parentElement.removeChild(this[i]);
        }
    }
};



