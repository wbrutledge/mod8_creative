// Require the functionality we need to use:
var http = require('http'),
	url = require('url'),
	path = require('path'),
	mime = require('mime'),
	mysql = require('mysql'),
	fs = require('fs');
	
var allUsers = new Array();
var activeUsers = new Array();
var available = new Array();
var sockets = new Array();
var leaderboard = new Array();
var availableUserObjs = new Array();
var games = new Array();
var tempTable = new Array();

var hangmanDB = mysql.createConnection({
	host: 'localhost',
	database: 'hangman',
	user: 'brobot',
	password: 'newpassword'
});


var greeting = "Admin: Welcome to the Hangman chatroom!";
	
var app = http.createServer(function(req, resp){
	var filename = path.join(__dirname, "static", url.parse(req.url).pathname);
	(fs.exists || path.exists)(filename, function(exists){
		if (exists) {
			fs.readFile(filename, function(err, data){
				if (err) {
					// File exists but is not readable (permissions issue?)
					resp.writeHead(500, {
						"Content-Type": "text/plain"
					});
					resp.write("Internal server error: could not read file");
					resp.end();
					return;
				}
				// File exists and is readable
				var mimetype = mime.lookup(filename);
				resp.writeHead(200, {
					"Content-Type": mimetype
				});
				resp.write(data);
				resp.end();
				return;
			});
		}else{
			// File does not exist
			resp.writeHead(404, {
				"Content-Type": "text/plain"
			});
			resp.write("Requested file not found: "+filename);
			resp.end();
			return;
		}
	});
});

app.listen(3456);

var io = require('socket.io').listen(app);

io.sockets.on("connection", function(socket) {
	
	socket.on("popLobby", function(name){
		hangmanDB.query("select username, rank, wins, losses from users where username='"+name+"';")
			.on('result', function(data){
				socket.emit("myProfile", data);
			})
			.on('end', function(data) {
				//console.log(allUsers);	
			});
		leaderboard=[];
		hangmanDB.query("select rank, username, wins, losses from users order by rank asc limit 5;")
			.on('result', function(data){
				leaderboard.push(data);
			})
			.on('end', function(){
				socket.emit("leaderboardUpdate", leaderboard);
			});
	});
	
	socket.on("availableToPlay", function() {
		
	
		
	});
		//loop to get array of available opponents
		/*for (i=0; i<activeUsers.length; i++){
			if(available[i]==true){
				var thisName = activeUsers[i];
				hangmanDB.query("select rank, username, wins, losses from users where username='"+thisName+"' sort by rank asc")
					.on('result', function(data){
						availableUserObjs.push(data);
						socket.broadcast.emit("renderOpponent", leaderboard);
					})
					.on('end', function(){
						
					});	
			}
		};
		socket.emit("renderOpponents", leaderboard);
		*/
		
		


	socket.on("newcomer", function(name) {
		if(activeUsers.indexOf(name) == -1) {
                	activeUsers.push(name);
			available.push(true);
			sockets.push(socket.id);
			hangmanDB.query("select rank, username, wins, losses from users where username='"+name+"';")
				.on('result', function(data){
					availableUserObjs.push(data);
				})
				.on('end', function(){
					socket.broadcast.emit("updateAvailable", availableUserObjs);
					socket.emit("updateAvailable", availableUserObjs);
				});
				
			//console.log(activeUsers);
			//console.log(sockets);
			var i = activeUsers.length-1;
			socket.broadcast.emit("updatePlayers", i);
			socket.emit("updatePlayers", i);
			socket.emit("relaymessage", greeting); 
		}
		else{
			socket.emit("alreadyLoggedIn");
			return;
		}
	});
	
	socket.on("sendmessage", function(content) {
		socket.broadcast.emit("relaymessage", content);
	});

	socket.on("disconnect", function() {
		var deadsocket = socket.id;
		var index = sockets.indexOf(deadsocket);
		sockets.splice(index,1);
		var name = activeUsers.splice(index,1);
		available.splice(index,1);
		availableUserObjs.splice(index,1);
		console.log(availableUserObjs);
		var i = activeUsers.length-1;
		socket.broadcast.emit("updatePlayers", i);
		socket.broadcast.emit("deleteAvailable", name);
		//console.log(activeUsers);
		//console.log(sockets);
		//console.log(available);
	});
	
	socket.on("challengingYou", function(opp, chal){
		//console.log("opp: "+opp);
		//console.log("chal: "+chal);
		var opponentIndex = activeUsers.indexOf(opp);
		var challengerIndex = activeUsers.indexOf(chal);
		var oppSocket = sockets[opponentIndex];
		//console.log(oppSocket);
		var chalSocket = sockets[challengerIndex];
		var data = [opp, oppSocket, chal, chalSocket];
		io.sockets.socket(oppSocket).emit("challengeMe", data);
	});
		
	socket.on("challengeAnswer", function(data, ans){
		if (ans==true) {
			//challenge accepted! initialize game between chal & opp at their sockets
			var chalSocket = data[3];
			var chal = data[2];
			var oppSocket = data[1];
			var opp = data[0];
			//remove players from available list
			var index = sockets.indexOf(chalSocket);
			//console.log("this is the index to remove: "+index);
			available.splice(index,1);
			availableUserObjs.splice(index,1);
			socket.broadcast.emit("deleteAvailable", chal);
			socket.emit("deleteAvailable", chal);
			var index2 = sockets.indexOf(oppSocket);
			available.splice(index2,1);
			availableUserObjs.splice(index2,1);
			socket.broadcast.emit("deleteAvailable", opp);
			socket.emit("deleteAvailable", opp);
			
			/*	
			//make socket info relative (me vs opp)
			oppData[0] = data[2];
			oppData[1] = data[3];
			oppData[2] = data[0];
			oppData[3] = data[1];
			*/
	
			game = {
				id: ""+data[2]+"_"+data[0],
				chal: data[2],
				opp: data[0],
				chalSocket: data[3],
				oppSocket: data[1],
				chalScore: 0,
				oppScore: 0
			};
			games.push(game);
			console.log(games);
			//send out info and begin game!
			io.sockets.socket(chalSocket).emit("initializeGame", game);
			io.sockets.socket(oppSocket).emit("initializeGame", game);
		}
		else{
			var chalSocket = data[3];
			var dataOut = data;
			console.log("sending denial to "+chalSocket);
			//challenge denied, send rejection notification.
			io.sockets.socket(chalSocket).emit("challengeDenied", dataOut);
		};
	});
	
	
	
	
	// server side for game
	socket.on("submitNewWord", function(game, who, newWord){
		var fixedWord = newWord.replace(/\s/g,'');
		fixedWord = fixedWord.toUpperCase();
		//check and see if word is valid
		var array = fs.readFileSync("static/dict.txt").toString().split('\n');
		if ((!(array.indexOf(fixedWord) == -1))&&(fixedWord.length<16)) {
			//create word array of characters
			var wordArray = fixedWord.split("");
			//word is valid, pass on to other user
			if (who=="chal") {
				console.log("1");
				io.sockets.socket(game.chalSocket).emit("receiveNewWord_Watch", wordArray, fixedWord);
				io.sockets.socket(game.oppSocket).emit("receiveNewWord", wordArray, fixedWord);
			};
			if (who=="opp") {
				console.log("2");
				io.sockets.socket(game.oppSocket).emit("receiveNewWord_Watch", wordArray, fixedWord);
				io.sockets.socket(game.chalSocket).emit("receiveNewWord", wordArray, fixedWord);
			};
		}
		else{
			//or else its not, try again
			if (who=="chal") {
				io.sockets.socket(game.chalSocket).emit("invalidWord");
			}
			if (who=="opp") {
				io.sockets.socket(game.oppSocket).emit("invalidWord");
			}
		};
	});
	
	
	socket.on("guessLetter", function(game, who, letter, wordArray, guesses, correct){
		//check if the letter is in the word
		var test = wordArray.indexOf(letter);
		if (test==-1) {
			//wrong guess!
			var guessNew = guesses - 1;
			if (guessNew>0) {
				//not over, keep trying
				if (who=="chal") {
					io.sockets.socket(game.chalSocket).emit("wrongGuess", letter, guessNew);
					io.sockets.socket(game.oppSocket).emit("wrongGuess_Watch", letter, guessNew);
				}
				if (who=="opp") {
					io.sockets.socket(game.oppSocket).emit("wrongGuess", letter, guessNew);
					io.sockets.socket(game.chalSocket).emit("wrongGuess_Watch", letter, guessNew);
				}
			}
			else{
				//fail! no point for you!
				if (who=="chal") {
					//no points
					io.sockets.socket(game.chalSocket).emit("wordFail", letter, wordArray);
					io.sockets.socket(game.oppSocket).emit("wordFail_Watch", letter, wordArray);
				}
				if (who=="opp") {
					//no points
					io.sockets.socket(game.oppSocket).emit("wordFail", letter, wordArray);
					io.sockets.socket(game.chalSocket).emit("wordFail_Watch", letter, wordArray);
				}
			}
		}
		if (test>=0){
			//right guess! but find all occurances of the letter in the array
			var j = 0;
			for (i=0;i<wordArray.length;i++) {
				if (letter==wordArray[i]) {
					j++;
				}
			}
			var updatedCorrect = correct + j;
						
			if (updatedCorrect<wordArray.length) {
			//keep going!
				if (who=="chal") {
					io.sockets.socket(game.chalSocket).emit("rightGuess", letter, wordArray, updatedCorrect);
					io.sockets.socket(game.oppSocket).emit("rightGuess_Watch", letter, wordArray, updatedCorrect);
				}
				if (who=="opp") {
					io.sockets.socket(game.oppSocket).emit("rightGuess", letter, wordArray, updatedCorrect);
					io.sockets.socket(game.chalSocket).emit("rightGuess_Watch", letter, wordArray, updatedCorrect);
				};
			}
			if (updatedCorrect==wordArray.length) {
				//you did it! points for you!
				if (who=="chal") {
					//add points
					game.chalScore++;
					io.sockets.socket(game.oppSocket).emit("wordWin_Watch", letter, wordArray, game);
					io.sockets.socket(game.chalSocket).emit("wordWin", letter, wordArray, game);
				}
				if (who=="opp") {
					//add points
					game.oppScore++;
					io.sockets.socket(game.chalSocket).emit("wordWin_Watch", letter, wordArray, game);
					io.sockets.socket(game.oppSocket).emit("wordWin", letter, wordArray, game);
				};
			};
		};
	});
	
	socket.on("gameover", function(winner, game, wins){
		if (winner=="chal") {
			//give a victory to chal and a loss to opp, offer rematch to opp
			console.log("winner is chal!")
			var me = game.chal;
			console.log("me-"+me);
			var newWins=wins+1;
			hangmanDB.query("update users set wins='"+newWins+"' where username='"+me+"';");

			reSortRanks();
			socket.emit("askPopLobby");
			socket.broadcast.emit("askPopLobby");
						
			var indexTemp = activeUsers.indexOf(game.opp)
			available[indexTemp]=true;
			hangmanDB.query("select rank, username, wins, losses from users where username='"+me+"';")
				.on('result', function(data){
					availableUserObjs.push(data);
				})
				.on('end', function(){
					socket.broadcast.emit("updateAvailable", availableUserObjs);
					socket.emit("updateAvailable", availableUserObjs);
				});
			io.sockets.socket(game.oppSocket).emit("returnToLobbyWin");
		};
		if (winner=="opp") {
			//give a victory to opp and a loss to chal, offer rematch to chal
			console.log("winner is opp!")
			var me = game.opp;
			console.log("me-"+me);
			var newWins=wins+1;
			hangmanDB.query("update users set wins='"+newWins+"' where username='"+me+"';");

			reSortRanks();
			socket.emit("askPopLobby");
			socket.broadcast.emit("askPopLobby");
						
			
			var indexTemp = activeUsers.indexOf(game.opp)
			available[indexTemp]=true;
			hangmanDB.query("select rank, username, wins, losses from users where username='"+me+"';")
				.on('result', function(data){
					availableUserObjs.push(data);
				})
				.on('end', function(){
					socket.broadcast.emit("updateAvailable", availableUserObjs);
					socket.emit("updateAvailable", availableUserObjs);
				});
			socket.emit("returnToLobbyWin");
		};
	});
	
	socket.on("iLost", function(who, name, losses, revenge){
		var RevengeYo = revenge;
		var newLosses=losses+1;
		hangmanDB.query("update users set losses='"+newLosses+"' where username='"+name+"';");
		
	
			var indexTemp = activeUsers.indexOf(game.opp)
			available[indexTemp]=true;
			hangmanDB.query("select rank, username, wins, losses from users where username='"+name+"';")
				.on('result', function(data){
					availableUserObjs.push(data);
				})
				.on('end', function(){
					socket.broadcast.emit("updateAvailable", availableUserObjs);
					socket.emit("updateAvailable", availableUserObjs);
				});
			socket.emit("returnToLobbyLoss", RevengeYo);
	});
	
	

});

function selectAllUsers(){
	console.log("gonna query");
	allUsers = hangmanDB.query('select * from users');
}

function reSortRanks() {
	tempTable=[];
	hangmanDB.query("select username from users order by wins desc;")
		.on('result', function(data){
			tempTable.push(data.username);
		})
		.on('end', function(){
			reReSortRanks(tempTable);
		});
};
function reReSortRanks(tmptbl){
	console.log("tempTable: "+tmptbl);
	for(var i=0;i<tmptbl.length;i++){
		var j = i+1;
		var name = tmptbl[i];
		hangmanDB.query("update users set rank='"+j+"' where username='"+name+"';");
	};	
}

function getUserObject(name){
	//given the username string, return that user's object literal with all data
	return hangmanDB.query("select rank, username, wins, losses from users where username='"+name+"';");
}