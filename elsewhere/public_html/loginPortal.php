<?php

    require '../../../notouch/databaseAccess-hangman.php';
    
    $username =$_POST['username'];
    $password =$_POST['password'];

    if ( (empty($_POST['username'])) || (empty($_POST['password'])) ){
        //No fields can be blank
        header("Location: index.php?error-note=0");
            exit;
    }    
    
    if( !preg_match('/^[\w_\-]+$/', $username) ){
        //Invalid username
        header("Location: index.php?error-note=2");
        exit;
    }
    
    //fetch username and passwordHash
    $stmt = $mysqli->prepare("select username, passwordHash from users where username=?");
        $stmt->bind_param('s', $username);
        $stmt->execute();
        $stmt->bind_result($usrnm, $psswdHash);
        $stmt->fetch();
        //echo $psswdHash;
        
        if (0==(strcmp($username, $usrnm))){        
            $passwordCrypt = crypt($password, $psswdHash);
            //check password and username for a match
            if ($psswdHash==$passwordCrypt) {
                //Successful log in
                session_start();
                //$_SESSION['username'] = $username;
                //$_SESSION['token'] = substr(md5(rand()), 0, 10);
                setcookie("usernameCookie", $username, time()+3600, "/");
                header("Location: http://ec2-54-200-63-85.us-west-2.compute.amazonaws.com:3456/home.html");
                //exit;
            }
            else{
                //Password not a match
                header("Location: index.php?error-note=4");
                exit;
            }
        }
        else{
            //Username is not a match.
            header("Location: index.php?error-note=3");
            exit;
        }


?>