<!DOCTYPE html>
<html>
<head>
	<title>HangBot! Login</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<link rel="stylesheet" href="style.css" type="text/css" media="screen" />
	<script type="text/javascript" src="//cdn.sencha.io/ext-4.1.1-gpl/ext-all-dev.js"></script>

</head>
<body>

<div id="login-wrap">
	
	<div id="logo"> HangBot! </div>
	<div id="login"> Log in</div>
	
	<div id="error-alert">
		<?php
                        if(@ $_GET['error-note']=="0"){
                            echo @"Please enter both your username and password.";
                        }
                        if(@ $_GET['error-note']=="1"){
                            echo "New user account created. Log in using the information you just provided.";
                        }
                        if(@ $_GET['error-note']=="q"){
                            echo "You have been logged out.";
                        }
                        if(@ $_GET['error-note']=="2"){
                            echo "Username is an invalid format. Please try again.";
                        }
                        if(@ $_GET['error-note']=="3"){
                            echo "Username not found. Remember, it's case sensitive.";
                        }
                        if(@ $_GET['error-note']=="4"){
                            echo "Username and password do no match. Please try again.";
                        }
			if(@ $_GET['error-note']=="5"){
                            echo "You are logged in somewhere else.";
                        }
                ?>
	</div>

	
	<form id="login-form" action="loginPortal.php" method="POST">
		<label class="login-label">Username:</label> <input class="login-input" type="text" name="username"></input>
		<label class="login-label">Password:</label> <input class="login-input" type="password" name="password"></input>
		<input type="submit" id="login-button" name="submit-login-attempt" value="Let's go!"></input>
	</form>
	
	
	
	

	<div id="register-link">
		Don't have an account? <a href="register.php"> Register </a>
	</div>
	
</div>


	

</body>
</html>
