<!DOCTYPE html>
<html>
<head>
	<title>HangBot! Register</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<link rel="stylesheet" href="style.css" type="text/css" media="screen" />
	<script type="text/javascript" src="//cdn.sencha.io/ext-4.1.1-gpl/ext-all-dev.js"></script>

</head>
<body>

<div id="login-wrap">
	
	<div id="logo"> HangBot! </div>
	<div id="login"> Register</div>
	
	<div id="error-alert">
	    <?php
                if(@ $_GET['error-note']=="0"){
                    echo "Please complete all fields.";
                }
                 if(@ $_GET['error-note']=="1"){
                    echo "Username contains invalid character(s). Please try again.";
                 }
                if(@$_GET['error-note']=="2"){
                    echo "Passwords do not match. Try again.";
                }
                if(@ $_GET['error-note']=="3"){
                    echo "Username already taken. Try a different username.";
                }
            ?>
	</div>

	
	<form id="login-form" action="registerPortal.php" method="POST">
		<label class="login-label">Desired username:</label> <input class="login-input" type="text" name="username"></input>
		<label class="login-label">Password:</label> <input class="login-input" type="password" name="password"></input>
		<label class="login-label">Verify Password:</label> <input class="login-input" type="password" name="passwordVerify"></input>
		<input type="submit" id="login-button" name="submit-login-attempt" value="Get started!"></input>
	</form>
	
	
	
	

	<div id="register-link">
		Already have an account? <a href="index.php"> Login </a>
	</div>
	
</div>

</body>
</html>
